package com.example.modul3;
import android.content.Context;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

public class AdapterUser extends RecyclerView.Adapter<AdapterUser.ViewHolder> {
    private ArrayList<User> listUser;
    private Context mContext;

    public AdapterUser(ArrayList<User> listUser, Context mContext) {
        this.listUser = listUser;
        this.mContext = mContext;
    }

    @Override
    public AdapterUser.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(AdapterUser.ViewHolder viewHolder, int list) {
        User currentUser = listUser.get(list);
        viewHolder.bindTo(currentUser);
    }

    @Override
    public int getItemCount() {
        return listUser.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView nama, pekerjaan;
        private ImageView poto;
        private int avatarCode;

        public ViewHolder(View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.txt_nama);
            pekerjaan = itemView.findViewById(R.id.txt_pekerjaan);
            poto = itemView.findViewById(R.id.avatar);

            itemView.setOnClickListener(this);
        }

        void bindTo(User currentUser){
            nama.setText(currentUser.getNama());
            pekerjaan.setText(currentUser.getPekerjaan());

            avatarCode = currentUser.getAvatar();
            switch (currentUser.getAvatar()){
                case 1 :
                    poto.setImageResource(R.drawable.ic_male);
                    break;
                case 2 :
                default:
                    poto.setImageResource(R.drawable.ic_female);
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            Intent toDetailActivity = new Intent(v.getContext(),DetailActivity.class);
            toDetailActivity.putExtra("nama",nama.getText().toString());
            toDetailActivity.putExtra("gender",avatarCode);
            toDetailActivity.putExtra("pekerjaan",pekerjaan.getText().toString());
            v.getContext().startActivity(toDetailActivity);
        }
    }

}
