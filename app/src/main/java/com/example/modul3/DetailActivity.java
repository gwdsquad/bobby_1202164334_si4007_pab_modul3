package com.example.modul3;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {
    private TextView namaDetail, pekerjaanDetail;
    private ImageView potoDetail;
    private int avatarCode;
    private String mNama,mPekerjaan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        namaDetail = findViewById(R.id.tx_detail_Nama);
        pekerjaanDetail = findViewById(R.id.tx_detail_Pekerjaan);
        potoDetail = findViewById(R.id.ig_detail_Avatar);

        mNama = getIntent().getStringExtra("nama");
        mPekerjaan = getIntent().getStringExtra("pekerjaan");
        avatarCode = getIntent().getIntExtra("gender",2);

        namaDetail.setText(mNama);
        pekerjaanDetail.setText(mPekerjaan);
        switch (avatarCode){
            case 1 :
                potoDetail.setImageResource(R.drawable.ic_male);
                break;
            case 2 :
            default:
                potoDetail.setImageResource(R.drawable.ic_female);
                break;
        }
    }
}

